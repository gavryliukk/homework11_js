'use strict'
const btn = document.querySelector(".btn");
const icon = document.querySelectorAll(".icon-password");

icon.forEach((e) => {
    e.addEventListener("click", () => {
        e.classList.toggle("fa-eye-slash");
        const inputType = e.previousElementSibling.getAttribute("type");
        if (inputType === "password") {
            e.previousElementSibling.setAttribute("type", "text");
        } else {
            e.previousElementSibling.setAttribute("type", "password");
        }
    });
});
let pass1 = document.querySelector('#pass1');
let pass2 = document.querySelector('#pass2');
let result = document.querySelector('.btn');

function checkPassword () {
    result.innerText = pass1.value === pass2.value ? alert('Welcome') : alert('Потрібно ввести однакові значення');
}

pass1.addEventListener('keyup', () => {
    if (pass2.value.length !== 0) checkPassword();
})

pass2.addEventListener('keyup', checkPassword);